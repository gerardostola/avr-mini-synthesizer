#include <avr/io.h>
#include <math.h>
#include <stdbool.h>

#include "tone.h"
tone_handle_type _tone;

tone_result_enum tone_init (tone_handle_type **h, unsigned long f_cpu, unsigned int gpio)
{
	tone_result_enum result = TONE_RESULT_ERROR;
	bool valid = false;
		
	switch (gpio)
	{
		case PORTB1:
			DDRB |= (1 << DDB1); // PB1 as output
			valid = true;
			break;		 
		default:
		case PORTB2:
			DDRB |= (1 << DDB2); // PB2 as output
			valid = true;
			break;
	}
	
	if (valid)
	{
		*h= &_tone;
		(*h)->gpio = gpio;
		// none-inverting mode
		TCCR1A |= (1 << COM1A1);
		// Mode 14 Fast PWM mode using ICR1 as TOP
		TCCR1A |= (1 << WGM11);
		TCCR1B |= (1 << WGM12)|(1 << WGM13);
		
		(*h)->f_cpu = f_cpu;
		(*h)->note_on = false;
		result = TONE_RESULT_OK;
	}
	return result;
}

void tone_setFrequency(tone_handle_type *h)
{
	double base, exponent;
	base = 2.0;
	exponent = (h->key-49.0) / 12.0;
	h->freq = TONE_A4_FREQ * pow (base, exponent) ;
}

void tone_setPrescaler(tone_handle_type *h)
{
	if ( TONE_NO_KEY == h->key )
	{
		//timer disabled
		TCCR1B &= ~(1 << CS12)&~(1 << CS11)&~(1 << CS10);
	}
	else
	{	
		if (h->key >= TONE_C4_KEY)
		{
			//No prescaling (x1)
			h->prescaler = 1.0;
			TCCR1B |= (1 << CS10);
			TCCR1B &= ~(1 << CS12)&~(1 << CS11);		
		}
		else
		{
			//Clock / 64
			h->prescaler = 64.0;
			TCCR1B |= (1 << CS11)|(1 << CS10);
			TCCR1B &= ~(1 << CS12);	
		}
	}
}

tone_result_enum tone_play (tone_handle_type *h, unsigned char key)
{
	tone_result_enum result = TONE_RESULT_ERROR;
	 
	if (key >= TONE_FIRST_KEY && key <= TONE_LAST_KEY)
	{
		if (!h->note_on)
		{
			h->note_on = true;
		}

		if (h->key != key)
		{
			h->key = key;
			tone_setFrequency(h);
			tone_setPrescaler(h);		
			ICR1 = (unsigned int) round(h->f_cpu/ (h->prescaler * h->freq)) -1;

			switch (h->gpio)
			{
				case PORTB1:				
					OCR1A = ICR1/2;	
				break;
				case PORTB2:
					OCR1B = ICR1/2;
				break;
			}
		}

		result = TONE_RESULT_OK;
	}
	return result;
}

void tone_stop(tone_handle_type *h)
{
	if (h->note_on)
	{
		h->note_on = false;
		h->key = TONE_NO_KEY;
		tone_setPrescaler(h);		
	}	
}
