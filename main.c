#define	F_CPU 16000000UL

#include <avr/io.h>
#include <stddef.h>
#include <avr/interrupt.h>
#include "tone.h"
#include "midi.h"
#include <util/delay.h>

tone_handle_type *tone;


void onKeyPressed(unsigned char midi_pitch, unsigned char velocity)
{
	unsigned char pitch = midi_pitch - 20; 
	/* velocity ignore for this simplistic implementation,
	given that the tone generator is a PWM signal with a fixed
	mean value (duty cycle=50%) */
	tone_play(tone, pitch);
}

void onKeyReleased(unsigned char midi_pitch, unsigned char velocity)
{
	/* pitch and velocity are ignored for this 
	simplistic implementation, given that the tone generator
	is monophonic, so it just turns the oscillator off */
	tone_stop(tone);
}


int main(void)
{	
	
	midi_handle_type *midi;
	
	tone_init(&tone, F_CPU, PORTB1);	
	midi_init(&midi, F_CPU, &onKeyPressed, &onKeyReleased);
	
	sei();

	while (1)
	{
		
	}
}