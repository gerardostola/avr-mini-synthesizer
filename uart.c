#include <avr/io.h>
#include <avr/interrupt.h>
#include "uart.h"

static uart_handle_type _uart;

ISR(USART_RX_vect)
{
	char r = UDR0;
	_uart.onByteReceived(r);
}

void uart_init(uart_handle_type **h, unsigned long f_cpu,
				unsigned int baudrate, 
				uart_OnByteReceived_t *onByteReceived)
{
	*h = &_uart;
	(*h)->onByteReceived=onByteReceived;

	unsigned int prescaler = (((f_cpu / (baudrate * 16UL))) - 1);
	(*h)->onByteReceived=onByteReceived;
	
	UBRR0L = (unsigned char)(prescaler & 0xff);
	UBRR0H = (unsigned char)(prescaler >> 8);

	/* enable receiver, interrupt on receive */
	UCSR0B = (1 << RXCIE0) |(1 << RXEN0);

	/* 8N1,  asynchronous  */
	UCSR0C =	(0 << UPM01) | (0 << UPM00) |
	(0 << UMSEL01) | (0 << UMSEL00) |
	(0 << USBS0) |
	(1 << UCSZ01) | (1 << UCSZ00);
}
