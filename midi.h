#include "uart.h"

typedef void (midi_OnKey_t)(unsigned char pitch, unsigned char velocity);

typedef struct 
{
	uart_handle_type *uart;
	midi_OnKey_t *onKeyPressed;
	midi_OnKey_t *onKeyReleased;
} midi_handle_type;

void midi_init(midi_handle_type **h, unsigned long f_cpu,
				midi_OnKey_t *onKeyPressed, 
				midi_OnKey_t *onKeyReleased);