#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "midi.h"
#include "uart.h"


#define MIDI_BAUDRATE 31250
#define MIDI_BUFFER_SIZE 8

midi_handle_type _midi;

/* Callback executed when a byte is received*/
void onByteReceived(char rx)
{
	static char buff[MIDI_BUFFER_SIZE];
	static unsigned int i = 0;						//buffer index
	static bool keyPressedFlag = false;
	static bool keyReleasedFlag = false;
	char code;
	//char channel;
	if ( rx&0x80 )
	{
		/* Status byte */		
		i = 0; /* resets the buffer */
		/* mask to obtain the higher nibble containing the code */
		code = rx & 0xf0;
		/* all channels are sniffed, no need to filter out */
		// channel = rx & 0x0f;
	
		switch (code)
		{
			case 0x90:
				keyPressedFlag = true;
				keyReleasedFlag = false;
				break;
			case 0x80:
				keyPressedFlag = false;
				keyReleasedFlag = true;
				_midi.onKeyReleased(0, 0);
				break;
		}
	}
	else
	{
		/* Data bytes */
		if ( i>=MIDI_BUFFER_SIZE )
		{
			/* Buffer full and nobody 
			retrieved the message, discard */
			i = 0;
		}	
		buff[i++] = rx;
		if (2==i)
		{
			if (keyPressedFlag )
			{					/* (pitch, velocity) */
				if (buff[1])
				{
					_midi.onKeyPressed(buff[0], buff[1]);	
				}
				else
				{
					/* On some devices, the note off is implemented
					by sending two bytes: key and velocity, this last 
					with value zero.
					*/   
					_midi.onKeyReleased(buff[0], 0);
					keyPressedFlag = false;
					keyReleasedFlag = false;
				}					
				/* Empties the buffer */
				i = 0;
			}
			if (keyReleasedFlag)
			{					/* (pitch, velocity) */
				_midi.onKeyReleased(buff[0], buff[0]);
				keyReleasedFlag = false;
				keyPressedFlag = false;

				/* Empties the buffer */
				i = 0;
			}
		}
	}
}

void midi_init(midi_handle_type **h, unsigned long f_cpu,
		 midi_OnKey_t *onKeyPressed, midi_OnKey_t *onKeyReleased)
{
	*h = &_midi;
	(*h)->onKeyPressed = onKeyPressed;
	(*h)->onKeyReleased = onKeyReleased;

	uart_init(&(*h)->uart, f_cpu, MIDI_BAUDRATE, &onByteReceived);

}
