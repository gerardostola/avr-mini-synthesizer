#include <stdbool.h>
#define TONE_NO_KEY		0		// For disabling the timer
#define TONE_FIRST_KEY	1		// piano's A0
#define TONE_D1_KEY		6		// piano's D1		
#define TONE_A1_KEY		13		// piano's A1

#define TONE_C4_KEY		40		// piano's C4
#define TONE_LAST_KEY	88		// piano's C8
#define TONE_A4_FREQ	440.0	// Hz

typedef struct
{
	bool note_on;
	unsigned long f_cpu;
	double prescaler;
	unsigned int gpio;
	unsigned char key;
	double freq;
} tone_handle_type;

typedef enum
{
	TONE_RESULT_OK,
	TONE_RESULT_ERROR,
} tone_result_enum;

tone_result_enum tone_init (tone_handle_type **h, unsigned long f_cpu, unsigned int gpio);
tone_result_enum tone_play (tone_handle_type *h, unsigned char key);
void tone_stop(tone_handle_type *h);