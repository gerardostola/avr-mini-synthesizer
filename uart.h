#ifndef __UART_H
#define __UART_H
typedef void (uart_OnByteReceived_t)(char rx);

typedef struct
{
	uart_OnByteReceived_t *onByteReceived;
} uart_handle_type;


void uart_init(uart_handle_type **h, unsigned long f_cpu,
				uint16_t baudrate, uart_OnByteReceived_t *onByteReceived);
				
#endif