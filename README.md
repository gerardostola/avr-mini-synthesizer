# AVR-mini-synthesizer


Frequency calculation using Timer 1, 16 bits, Fast PWM, using ICR1 as TOP (Mode 14)

ICR1 = f(system) / (prescaler * f (desired) ) − 1 =

f(system) = 16E6 (Arduino Uno)
f(desired) = depends on the note, for example A 4th octave is 440 Hz; See http://donrathjr.com/wp-content/uploads/2010/07/Table-of-Frequencies1.png
OCR1A = ICR1 / 2 (50% duty cycle)

If prescaler is 1
4th octave
Key     ICR1   OCR1A
A      36362   18181
...


Given a piano key, this is the formula to convert it into frequency.
f = 440 * 2^((n−49)/12)

For lowest musical notes, the ICR1 calculation cannot be represented using 16 bits, so the prescaler is adjusted.
